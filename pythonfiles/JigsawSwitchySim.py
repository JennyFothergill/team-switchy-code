#Importing important things for our code to work correctly!
import hoomd
import hoomd.md
import json
import numpy as np

#Important variables so we don't have to scroll through all the code and change a bunch each time
runTime = 1e7
particles = 1
epsilon_sticky = 50
sigma_sticky = 1.1
dt = 0.005
kT = 0.1
xaxis = 10
yaxis = 10

#Initializing where our code will run
hoomd.context.initialize("");

#Creating a unitcell with three particles as our center. These are our rigid center particles. 
    #The a1,a2, and a3 define our box size.
    #The mass and moment of inertia were found by running init_wrapper on the trapezoid made in mbuild in a different file. 
uc = hoomd.lattice.unitcell(N = particles,
                           a1 = [18, 0, 0],
                           a2 = [0, 18, 0],
                           a3 = [0, 0, 0],
                           dimensions = 2,
                           position = [[0,0,0]],# [0,6,0], [0,12,0]],
                           type_name = ['_R0'],# '_R1', '_R2'],
                           mass = [45.0], # 45.0, 45.0],
                           moment_inertia = [[65.83333333, 322.5, 388.33333333]], ## [[4903.50392555,47580,52483.50392555]], 
                           ## [60.83333333, 812.5, 873.33333333]],
                           orientation = [[1,0,0,0]],# [1,0,0,0], [1,0,0,0]]);

#This replicates the rigid center particle 6 in the x direction and 2 in the y direction
system = hoomd.init.create_lattice(unitcell=uc, n = [xaxis,yaxis])

#Adding our two types of particles that we want. 
    #Z is normal particles
    #A,B,C,D,E,F are our sticky particles
system.particles.types.add('Z')
system.particles.types.add('A')
system.particles.types.add('B')
#system.particles.types.add('C')
#system.particles.types.add('D')
#system.particles.types.add('E')
#system.particles.types.add('F')

#This fills in the positions of our particles to make our trapezoid
    #All positions were found using init_wrapper in another file
# Not on fry:
#with open("/Users/Rachel/VIP_Stuff/team-switchy-code/jupyterfiles/rigid_info_jigsaw.json") as json_data:
# On fry:
with open("/home/rachelsingleton/team-switchy-code/jupyterfiles/rigid_info_jigsaw.json") as json_data:
         rigid_body_data = json.load(json_data)
particle_positions = rigid_body_data[0]["r_positions"] 

particle_positions = np.array(particle_positions)
#This divides the positions by 10 so they aren't so spread out and look nicer in the simulations
particle_positions /= 10

#Creates the rigid body but nothing is in it yet
rigid = hoomd.md.constrain.rigid()

#Puts the particles in their correct positions based on their types
bottom_right = 'Z'
right_b_mid = 'Z'
bottom4Rmid = 'Z'
bottom3Rmid = 'Z'
right_middle = 'Z'
right_u_mid = 'Z'
top_right = 'Z'
bottom2Rmid = 'Z'
top2Rmid = 'Z'
bottom1Rmid = 'Z'
top1Rmid = 'A'
bottom_middle = 'Z'
top_middle = 'A'
bottom1Lmid = 'Z'
top1Lmid = 'Z'
bottom2Lmid = 'Z'
top2Lmid = 'Z'
top_left = 'Z'
bottom4Lmid = 'Z'
left_u_mid = 'Z'
bottom_left = 'Z'
left_b_mid = 'B'
bottom3Lmid = 'Z'
left_middle = 'B'
rigid.set_param('_R0',
               types=[bottom_right, bottom4Rmid, right_b_mid, bottom3Rmid, 'Z', right_middle, right_u_mid, top_right, bottom2Rmid, 'Z', 'Z', 'Z', top2Rmid, bottom1Rmid, 'Z', 'Z', 'Z', top1Rmid, bottom_middle, 'Z', 'Z', 'Z', top_middle, bottom1Lmid, 'Z', 'Z', 'Z', top1Lmid, bottom2Lmid, 'Z', 'Z', 'Z', top2Lmid, bottom3Lmid, 'Z', 'Z', 'Z', top_left, bottom4Lmid, 'Z', 'Z', left_u_mid, bottom_left, left_b_mid, left_middle],
               #Sets positions of constituent particles
               positions=particle_positions)

rigid.create_bodies()

#Identifying our neighborlist
nl = hoomd.md.nlist.cell()

#Lennard Jones potential is specified with the desired r_cut value
lj = hoomd.md.pair.lj(r_cut=2.5, nlist=nl)

#Helps not cut the potential off so abruptly
lj.set_params(mode='xplor')

#Sets the interactions between the different groups of particles.

#Interations dealing with just rigid centers
lj.pair_coeff.set('_R0', '_R0', epsilon=0, sigma=0, r_cut=False)
#lj.pair_coeff.set('_R1', '_R1', epsilon=0, sigma=0, r_cut=False)
#lj.pair_coeff.set('_R1', '_R2', epsilon=0, sigma=0, r_cut=False)
#lj.pair_coeff.set('_R0', '_R1', epsilon=0, sigma=0, r_cut=False)
#lj.pair_coeff.set('_R0', '_R2', epsilon=0, sigma=0, r_cut=False)
#lj.pair_coeff.set('_R2', '_R2', epsilon=0, sigma=0, r_cut=False)

#Interactions dealing with particle A
lj.pair_coeff.set('A','A', epsilon=0, sigma=0, alpha=0)
lj.pair_coeff.set('A', '_R0', epsilon=0, sigma=0)
#lj.pair_coeff.set('A', '_R1', epsilon=0, sigma=0)
#lj.pair_coeff.set('A', '_R2', epsilon=0, sigma=0)
lj.pair_coeff.set('A', 'B', epsilon=0, sigma=0, alpha=0)
#lj.pair_coeff.set('A', 'C', epsilon=0, sigma=0, alpha=0)
#lj.pair_coeff.set('A', 'D', epsilon=0, sigma=0, alpha=0)
#lj.pair_coeff.set('A', 'E', epsilon=0, sigma=0, alpha=0)
#lj.pair_coeff.set('A', 'F', epsilon=epsilon_sticky, sigma=sigma_sticky)
lj.pair_coeff.set('A', 'Z', epsilon=2, sigma=0.9, alpha=0) #First trap was 1, 0.5
#
##Interactions dealing with particle B
lj.pair_coeff.set('B','B', epsilon=0, sigma=0, alpha=0)
lj.pair_coeff.set('B', '_R0', epsilon=0, sigma=0)
#lj.pair_coeff.set('B', '_R1', epsilon=0, sigma=0)
#lj.pair_coeff.set('B', '_R2', epsilon=0, sigma=0)
#lj.pair_coeff.set('B', 'C', epsilon=epsilon_sticky, sigma=sigma_sticky)
#lj.pair_coeff.set('B', 'D', epsilon=0, sigma=0, alpha=0)
#lj.pair_coeff.set('B', 'E', epsilon=0, sigma=0, alpha=0)
#lj.pair_coeff.set('B', 'F', epsilon=0, sigma=0, alpha=0)
#lj.pair_coeff.set('B', 'Z', epsilon=2, sigma=0.9, alpha=0)
#
##Interactions dealing with particle C
#lj.pair_coeff.set('C','C', epsilon=0, sigma=0, alpha=0)
#lj.pair_coeff.set('C', '_R0', epsilon=0, sigma=0)
#lj.pair_coeff.set('C', '_R1', epsilon=0, sigma=0)
#lj.pair_coeff.set('C', '_R2', epsilon=0, sigma=0)
#lj.pair_coeff.set('C', 'D', epsilon=0, sigma=0, alpha=0)
#lj.pair_coeff.set('C', 'E', epsilon=0, sigma=0, alpha=0)
#lj.pair_coeff.set('C', 'F', epsilon=0, sigma=0, alpha=0)
#lj.pair_coeff.set('C', 'Z', epsilon=2, sigma=0.9, alpha=0)
#
##Interactions dealing with particle D
#lj.pair_coeff.set('D','D', epsilon=0, sigma=0, alpha=0)
#lj.pair_coeff.set('D', '_R0', epsilon=0, sigma=0)
#lj.pair_coeff.set('D', '_R1', epsilon=0, sigma=0)
#lj.pair_coeff.set('D', '_R2', epsilon=0, sigma=0)
#lj.pair_coeff.set('D', 'E', epsilon=epsilon_sticky, sigma=sigma_sticky)
#lj.pair_coeff.set('D', 'F', epsilon=0, sigma=0, alpha=0)
#lj.pair_coeff.set('D', 'Z', epsilon=2, sigma=0.9, alpha=0)
#
##Interactions dealing with particle E
#lj.pair_coeff.set('E','E', epsilon=0, sigma=0, alpha=0)
#lj.pair_coeff.set('E', '_R0', epsilon=0, sigma=0)
#lj.pair_coeff.set('E', '_R1', epsilon=0, sigma=0)
#lj.pair_coeff.set('E', '_R2', epsilon=0, sigma=0)
#lj.pair_coeff.set('E', 'F', epsilon=0, sigma=0, alpha=0)
#lj.pair_coeff.set('E', 'Z', epsilon=2, sigma=0.9, alpha=0)
#
##Interactions dealing with particle F
#lj.pair_coeff.set('F','F', epsilon=0, sigma=0, alpha=0)
#lj.pair_coeff.set('F', '_R0', epsilon=0, sigma=0)
#lj.pair_coeff.set('F', '_R1', epsilon=0, sigma=0)
#lj.pair_coeff.set('F', '_R2', epsilon=0, sigma=0)
#lj.pair_coeff.set('F', 'Z', epsilon=2, sigma=0.9, alpha=0)

#Interactions dealing with particle Z
lj.pair_coeff.set('Z','Z', epsilon=2, sigma=0.9, alpha=0)
lj.pair_coeff.set('Z', '_R0', epsilon=0, sigma=0)
#lj.pair_coeff.set('Z', '_R1', epsilon=0, sigma=0)
#lj.pair_coeff.set('Z', '_R2', epsilon=0, sigma=0)
hoomd.md.integrate.mode_standard(dt=dt);

#Creates the rigid_center group and applies langevin forces to them
rigid_center = hoomd.group.rigid_center()
integrate = hoomd.md.integrate.langevin(group=rigid_center, kT=kT, seed=42);

#Writes our results into a .log file
hoomd.analyze.log(filename="runs/100_jigsaw_dense_6_7/jigsaw_asymmetric.log",
                 quantities=['potential_energy',
                            'translational_kinetic_energy',
                            'rotational_kinetic_energy'],
                 period=100,
                 overwrite=True);

#Writes the initial trapezoid into a file that we can see for the poster
hoomd.dump.gsd("runs/100_jigsaw_dense_6_7/initial_jigsaw_traj.gsd", period=None, group=hoomd.group.all(), overwrite=True)

#Writes the simulation into a .gsd file
hoomd.dump.gsd("runs/100_jigsaw_dense_6_7/jigsaw-traj.gsd",
              period=2e3,
              group=hoomd.group.all(),
              overwrite=True);

#Let's run it!
hoomd.run(runTime)
