import signac

def main():
    project = signac.init_project('Jigsaw-Signac')
    for kT in [0.1, 0.5]:
      for trap_type in ["Alpha", "Beta", "Gamma", "Delta", "Zeta", "Eta", "Iota", "Kappa", "Lamda", "Omicron"]:
        statepoint = {'kT':kT}
        statepoint["sticky"] = True
        statepoint["trap_type"] = trap_type
        job = project.open_job(statepoint)
        job.init()

if __name__ == '__main__':
    main()
