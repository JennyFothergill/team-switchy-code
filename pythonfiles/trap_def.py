# This will have all of the different trapezoids that we want to test with. 
# We will then import this file into project.py

# GENERAL TRAPEZOID SO I KNOW WHERE THE PARTICLES ARE:
# [bottom_right, bottom4Rmid, right_b_mid, bottom3Rmid, 'Z', right_middle, right_u_mid, top_right, bottom2Rmid, 'Z', 'Z', 'Z', top2Rmid, bottom1Rmid, 'Z', 'Z', 'Z', top1Rmid, bottom_middle, 'Z', 'Z', 'Z', top_middle, bottom1Lmid, 'Z', 'Z', 'Z', top1Lmid, bottom2Lmid, 'Z',     'Z', 'Z', top2Lmid, bottom3Lmid, 'Z', 'Z', 'Z', top_left, bottom4Lmid, 'Z', 'Z', left_u_mid, bottom_left, left_b_mid, left_middle]

trap_types={}

# Sticky Particles on top_right, top2Rmid, bottom_left, left_b_mid
trap_types["Alpha"] = ['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'C', 'Z', 'Z', 'Z', 'Z', 'C', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'C', 'C', 'Z']
# Sticky Particles on top_right, top2Rmid, top1Rmid, bottom_left, left_b_mid, left_middle
trap_types["Beta"] = ['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'C', 'Z', 'Z', 'Z', 'Z', 'C', 'Z', 'Z', 'Z', 'Z', 'C', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'C', 'C', 'C']
# Sticky Particles on top_right (A), top2Rmid (A), bottom_left (B), left_b_mid (B)
trap_types["Gamma"] = ['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'A', 'Z', 'Z', 'Z', 'Z', 'A', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'B', 'B', 'Z']
# Sticky Particles on top-middle (A), top1Rmid (A), left_b_mid (B), left_middle (B)
trap_types["Delta"] = ['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'A', 'Z', 'Z', 'Z', 'Z', 'A', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'B', 'B']
# Sticky Particles on top1Rmid (A), top2Rmid (A), left_b_mid (B), left_middle (B)
trap_types["Zeta"] = ['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'A', 'Z', 'Z', 'Z', 'Z', 'A', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'B', 'B']
# Sticky Particles on top-middle, top1Rmid, left_b_mid, left_middle
trap_types["Eta"] = ['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'C', 'Z', 'Z', 'Z', 'Z', 'C', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'C', 'C']
# Sticky Particles on top1Rmid, top2Rmid, left_b_mid, left_middle
trap_types["Iota"] = ['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'C', 'Z', 'Z', 'Z', 'Z', 'C', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'C', 'C']
# Sticky Particles on top_right (B), top2Rmid (A), bottom_left (A), left_b_mid (B)
trap_types["Kappa"] = ['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'B', 'Z', 'Z', 'Z', 'Z', 'A', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'A', 'B', 'Z']
# Sticky Particles on top-middle (A), top1Rmid (B), left_b_mid (A), left_middle (B)
trap_types["Lamda"] = ['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'A', 'Z', 'Z', 'Z', 'Z', 'B', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'A', 'B']
# Sticky Particles on top1Rmid (A), top2Rmid (B), left_b_mid (A), left_middle (B)
trap_types["Omicron"] = ['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'A', 'Z', 'Z', 'Z', 'Z', 'B', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'A', 'B']
